;
(function() {
    'use strict';
    var config = {
        publish_key: 'pub-c-495e0764-1778-43d2-9f03-c29b7e231b8a',
        subscribe_key: 'sub-c-89dcd64a-86ed-11e4-a400-02ee2ddab7fe',
        pubnubIncomingCallChannel: 'fd_router',
        //xmsDomain :  "54.152.232.25",
        xmsPort: "1080",
        myNumber: 'xms_972722156195'

    }
    angular.module('foneDo', []);
    angular.module('dailer', [
            "pubnub.angular.service",
            'foneDo.call',
            'foneDo',
            'angularModalService',
            "angular-locker"
        ])
        .config(['lockerProvider', 'configServiceProvider', function(lockerProvider, configServiceProvider) {
            configServiceProvider.setPubNubCredentials('pub-c-495e0764-1778-43d2-9f03-c29b7e231b8a', 'sub-c-89dcd64a-86ed-11e4-a400-02ee2ddab7fe');
            lockerProvider
                .setDefaultDriver('local')
                .setDefaultNamespace('fonedo')
                .setSeparator('.')
                .setEventsEnabled(false);
        }])
        .run([function() {
            console.log('app start running');

        }])
        .controller('dailerController', [
            '_', "locker", "$rootScope", '$scope', 'callHandler', "PubNub", 'xmsManager', 'configService', 'userService',
            function(_, locker, $rootScope, $scope, callHandler, PubNub, xmsManager, sConfig, sUser) {
                var previousChannel = undefined;
                var audio = document.getElementById('ringing');


                /*PubNub.init({
                      publish_key: config.publish_key,
                      subscribe_key: config.subscribe_key 
                  });*/


                $scope.xmsDomain = sConfig.xmsUrl;
                $scope.xmsPort = config.xmsPort;
                $scope.subscribeChannel = config.pubnubIncomingCallChannel;


                $scope.userToken = "";

                $scope.dailNumber = "";
                $scope.publishChannel = "MTQxNTMxNjA1NDk6YTAxYjRhZGExMmI0ODhmZGIwYTU2YWFiMjgzNTc2NWM6MWEwOTE1MWIyODE3ZTE1MTAyZDVmM2ViODJhMWY2M2QwOTJlODkyYmZiYjEwN2MxYjQ4YzJmM2QzOWE1NmNiNg==";
                $scope.publishChannel = "OTcyNzIyMTU2MTcxOmQ4MjE0YjM1MDc1MDA3Y2VkMjlkZGEyNmRiZDBjNjAwOjA5MjgzMTZiODI1MDYwNmRlZmFiM2FmM2Y1YjEzNjNhOGY0YmQzMDgyYTgzZDVhY2ZkODljYWI5YzZlNTQ4YmI=";
                $scope.publishChannel = "OTcyNzIyMTU2MjA5OjQ5YjdkMzUzYjk2YjQzMTk0ODk5ZmJmMjdiMTc5MmZkOjMyMGFhMmJiZDEzMjVkZjU0Y2QxYTA0MjJkNjQyMGRkNWUzMWYxNDM4OTljNGQzMDUzMjA2MGE0M2M4NzI5NGI=";
                $scope.publishChannel = "MTQxNTMxNjA1NDE6Mzc4OGIwNDkyMzNhMGMxN2JiZDZhOWZhMTY2MDc5YWM6NWJlNjNhZmFkNmIwMTg5OWI2MmU1Y2UxNjIwNmY4M2Q2ZGNmMDM4Zjk3ZDMzNGVkM2E0YmJmNDFkZmI5ZWM4MA==";
                $scope.subscribeChannel = $scope.publishChannel;

                $scope.isRinging = false;
                $scope.logDetails = [];
                $scope.restApi = 'qa.fone.do';
                $scope.callData = {
                    Direction : 'inbound',
                    CallStatus : 'ringing',
                    CallSid : '1',
                    From : '1',
                    To   : '4153160549', 
                    ConferenceRoom : "12"  
                };

                $scope.$watch('callData.CallSid',function(value){
                    $scope.callData.From = value;
                })

                $scope.plusOne = function(){
                    $scope.callData.CallSid++;
                }

                $scope.changeXms = function() {
                    xmsManager.setXmsPath($scope.xmsDomain, $scope.xmsPort)
                }

                $scope.chagneApi = function() {
                    sConfig.restApi = $scope.restApi;
                }

                $scope.publish = function() {
                    PubNub.ngPublish({
                        channel: $scope.publishChannel,
                        message: $scope.getPubNubMessage()
                    });
                }

                $scope.subscribe = function() {
                    if (previousChannel == $scope.subscribeChannel) {
                        return;
                    }
                    if (previousChannel != undefined) {
                        PubNub.ngUnsubscribe({
                            channel: theChannel
                        })
                    }

                    previousChannel = $scope.subscribedChannel;
                    // subscribe to the channel
                    PubNub.ngSubscribe({
                        channel: $scope.subscribeChannel
                    });

                    // handle message events
                    $rootScope.$on(PubNub.ngMsgEv($scope.subscribeChannel), function(event, message) {
                        $scope.handleIncomingCall(message);
                    });
                }

                $scope.handleIncomingCall = function(message){
                    $scope.logDetails.push(_.merge({},{id : $scope.logDetails.length },message.message));
                    $scope.$evalAsync();
                }

                $scope.setUserToken = function() {
                    locker.put('token', $scope.userToken);
                    //callHandler.init();
                }

                $scope.dail = function() {
                    console.log('dial', xmsManager.xmsPath())
                    var contact = {
                        contact: {
                            number: $scope.dailNumber
                        }
                    }
                    callHandler.outgoingCall(contact).then(function(result) {
                        console.log('call is set up', result);
                    });
                }

                $scope.accept = function() {
                    audio.pause();
                    $scope.isRinging = false;
                    xmsManager.register($scope.result.CallSid, "", $scope.xmsDomain, $scope.xmsPort)
                        .then(function() {
                            $scope.unRegisterToken = $scope.result.CallSid;
                            xmsManager.startCall($scope.result.CallSid)
                        }, function() {

                        })
                }

                $scope.reject = function() {
                    audio.pause();
                    $scope.isRinging = false;
                    $scope.hangup();
                }

                $scope.pullCall = function() {
                    xmsManager.register($scope.userToken, "", $scope.xmsDomain, $scope.xmsPort)
                        .then(function() {
                            $scope.unRegisterToken = $scope.userToken;
                            xmsManager.startCall($scope.userToken)
                        }, function() {

                        })
                }

                $scope.transfer = function() {

                }

                $scope.getPubNubMessage = function() {
                    return $scope.callData;
                }

                $scope.init = function() {

                    //PubNub.init(sConfig.getPubNubCredentials());
                    PubNub.init({
                        publish_key: config.publish_key,
                        subscribe_key: config.subscribe_key
                    });
                    $scope.subscribe();
                    //callHandler.init();
                    var data = {
                        phone: "12671231131",
                        user_id: "12671231131"
                    }
                    locker.put('userDetails', data)
                    sUser.setDetails(data);
                    $scope.setUserToken();
                }

                $scope.init();


            }
        ]);
}());
