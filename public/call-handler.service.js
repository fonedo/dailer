
/*
fonedo.token	"OTcyNzIyMTU2MTcxOjY5YmEzOGM1OGZmMjcxODZlNDYyYzY4NmZhNWU3M2JhOmUzOGFjZmZmMGQzN2QxODRmOTIwNjFlODAwYjQyMjEzNjc0ZDI2Y2VmNjVjNDY0N2U1YzBjYzY3NmQ2OTVmYjE="	
fonedo.userDetails	{"id":"972722156171","extension":102,"phone":"972722156171","full_name":"george","role":"user"}	

 */
var fonedo = fonedo || {};

if (!fonedo['services']) {
    fonedo.services = {};
}

/**
 *  @module foneDo
 *  @type service
 *  @name callHandler
 *  @description
 *
 * Call Manager listen for incoming / outgoing calls 
 * making live call modification 
 * controlls the display of the call modal
 */

fonedo.services.callHandler = (function() {
    'use strict';
    var config = {
        paths: {
            call: 'call/:callId',
            calls : 'calls'

        }
    }
    /*var _activeCall = {
        state: "",
        type : "",
        callSid: undefined
    };*/

    var _conference = {
        roomNumber : "",
        participants : [],
    }

    

    function callHandler($rootScope, _, $q, $http, $log,$timeout, modalService, xmsManager, sUser, sConfig, PubNub, sUtils,activeCall,callMembers,callActions) {
        var self = this;
        
        self.waitingCalls = [];
        self.conferenceList = [];
        self.notifications = [];
        self.modalStatus = 'hidden';
        
        /**
         * init the object 
         * set state
         * @return {[type]} [description]
         */
        self.init = function init() {
            $log.info('service is initialized');
            PubNub.ngSubscribe({
                channel: sConfig.chanelName + sUser.getToken(),
                callback : self.pubNubHandler
            });
            $rootScope.$on('xmsHandler:event',self.xmsHandlerCallback);
            sUtils.requestNotificationPermission();
            xmsManager.init(sConfig.xmsUrl,sConfig.xmsPort);
        }

        /**
         * get contact details 
         * @type {[type]}
         */
        self.getContact = function getContact (number){
            return {
                number : number
            }
        }
        
        /**
         * external function to make the call
         * @param  {[type]} number [description]
         * @return {[type]}        [description]
         */
        self.foneDoCall = function foneDoCall(number){
            if(!number){
                return;
            }
            var contact = self.getContact(number);

            switch (activeCall.data.status)
            {
                case sConfig.callState.idle:
                    self.outgoingCall({
                        contact : contact
                    });
                break;
                case sConfig.callState.transfer:
                    callAction.transferCall(contact);
                break;
                case sConfig.callState.conference:
                    self.serverConferenceCall({
                        contact : contact
                    });
                break;
            }
        }

        /**
         * starts and stop the ringing for incoming call
         * @param  {[type]} state [description]
         * @return {[type]}       [description]
         */
        self.ringingHandler = function ringingHandler(state) {
            var audio = document.getElementById('ring');
            if (state == 'play') {
                audio.play();
            } else {
                audio.pause();
            }
        };

        /**
         * generating outgoing call
         * @param  {[type]} callProperties [description]
         * @param  {[type]} style          [description]
         * @return {[type]}                [description]
         */
        self.outgoingCall = function outgoingCall(callProperties,style) {
            var deffered = $q.defer();
            var registerToken = callProperties.registerToken ? callProperties.registerToken : sUser.getToken();

            xmsManager.generateCall(registerToken,callProperties.contact.number)
                .then(function(result){
                        
                        callProperties.callControllType = sConfig.callControllType.call;
                        callProperties.callState = 'active';
                        if(!callProperties.callTargetTitle){
                            callProperties.stateInfoDisplay = "Dialing | "
                            callProperties.callTargetTitle = callProperties.contact.number;
                        }

                        var memberId= callMembers.addMember({
                            contact : callProperties.contact,
                            status : sConfig.callState.ringing,
                            state  : 'active',
                            type : sConfig.callType.outgoing,
                            callSid : undefined
                        });
                        
                        callProperties.memberId = memberId;
                        activeCall.activeMemberId = memberId;

                        self.openModal(callProperties,style);
                        deffered.resolve(activeCall);
                    },
                    function(result){
                        console.error('failed register to xms',message);
                        deffered.reject(message);
                    },
                    function(result){
                        
                        activeCall.create({
                            status : sConfig.callState.connecting,
                            type : sConfig.callType.outgoing,
                            registerToken : registerToken
                        });
                    });

            return deffered.promise;
        }

        /**
         * receiving incoming (Open the modal and set the call parameters)
         * @param  {[type]} callProperties [description]
         * @return {[type]}                [description]
         */
        self.incomingCall = function incomingCal(callProperties) {
           
            if (!callProperties.CallSid) {
                return;
            }
            var contact = self.getContact(callProperties.From);
            
            var memberId = callMembers.addMember({
                            contact : contact,
                            type : sConfig.callType.incoming,
                            callSid : callProperties.CallSid,
                            state : 'not-active'
                        });

            //move that to function
            var callData = {
                    id : memberId,
                    timeout : $timeout(function(){
                        $rootScope.$broadcast('waitingCall:remove',{memberId : memberId});
                    },sConfig.answeringTime)
            }
            
            if(self.modalStatus == 'hidden'){
                
                callMembers.updateMember(memberId,{status:sConfig.callState.answering});
                
                self.ringingHandler('play');
                
                callProperties.contact = contact;
                callProperties.memberId = memberId;
                callProperties.callState = 'answering';
                callProperties.stateInfoDisplay = "Incoming Call | " ;
                callProperties.callTargetTitle =  callProperties.From;
                callProperties.callControllType = sConfig.callControllType.answer;

                self.openModal(callProperties);
            } 
            else{
                callMembers.updateMember(memberId,{status:sConfig.callState.waiting});
                $rootScope.$broadcast('waitingCalls:add',callData);
            }
        }

        /**
         * creating connection with the xms for incoming calls 
         * @return {[type]} [description]
         */
        self.acceptCall = function acceptCall(guid){
            self.ringingHandler('pause');
            //clear the waiting call time out and remove it from the waiting call list
            var member = callMembers.getMember(guid);
            if(!member.callSid){
                return;
            }
            var notification = _.find(self.notifications,{callSid : member.callSid});
            if(notification){
                notification.notification.close();
            }
            xmsManager.generateCall(member.callSid,member.callSid)
                .then(null,null,function(){
                    activeCall.create({
                        status : sConfig.callState.connecting,
                        type : sConfig.callType.incoming,
                        registerToken : member.callSid,
                        callSid : member.callSid,
                        state   : 'singleMember'
                    });
                    self.waitingCallsRemoveItem(member.guid,'accept');    
                    member.state = 'active';
                }) 
        }

        self.openModal = function openModal(properties,style) {
            var defered = $q.defer();
            if(style == 'dialer'){
                var templateUrl = 'simple-call-modal.tpl.html';
            }else{
                var templateUrl = 'call-modal.tpl.html';
            }
            modalService.showModal({
                templateUrl: sConfig.callModalTemplateSource + templateUrl,
                controller: "callController",
                controllerAs: 'call',
                inputs: {
                    properties: properties
                }
            }).then(function(modal) {
                self.modalHandler = modal;
                self.modalStatus = 'visible';
                self.closeModal = modal.close.then(function(data) {
                    self.modalStatus = 'hidden';
                    self.modalHandler = undefined;
                    if(activeCall.data.status != sConfig.callState.idle){
                        self.clearCall();
                    }
                    callMembers.removeAll();
                    defered.resolve(data);
                });
            });
            return defered.promise;
        }
        
        self.parsePubNubMessage = function parsePubNubMessage(message) {
            var data = message;
            if (typeof data == 'string') {
                data = JSON.parse(data);
            }
            return data;
        };
        
        self.xmsHandlerCallback = function xmsHandlerCallback(event,data){
            switch(data.type){
                case 'hangup':
                    if(activeCall.data.status != sConfig.callState.idle){
                        $rootScope.$broadcast('callModal:close', {
                            status: activeCall,
                            initiator : data.initiator
                        });
                        self.clearCall();
                    }
                break;
                case 'connected':
                break;
                case 'ringing':
                    activeCall.status = sConfig.callState.ringing;
                    callMembers.updateMember(activeCall.activeMemberId,{status : sConfig.callState.ringing})
                break;
                case "remoteStreamAdded": 
                    activeCall.status = sConfig.callState.process;
                    callMembers.updateMember(activeCall.activeMemberId,{status : sConfig.callState.process})
                     $rootScope.$broadcast('callModal:remoteStreamAdded', {
                            status: activeCall
                        });
                break;
            }
        }

        self.pubNubHandler = function pubNubHandler(event,payload) {
            /**
                CallSid: "CAae20e9ef39954b05ad11f55373964fe5"
                CallStatus: "ringing"
                From: "xms_1238"
                To: "972527122336"
             */
            var message = self.parsePubNubMessage(payload[0][0]);
            console.debug('pub nub message',message);
            switch (message.CallStatus.toLowerCase()) {
                case "ringing":
                    //checks if it is not my current outgoing call
                    if(message.From.indexOf(sUser.phone) > -1){
                            //only if my app made the call
                            if(activeCall.data.status != sConfig.callState.idle){
                                activeCall.data.callSid = message.CallSid;
                                callMembers.updateMember(activeCall.data.activeMemberId,{callSid : message.CallSid});
                                $rootScope.$broadcast('callModal:updateCallSid',{callSid : message.CallSid });    
                            }

                    }
                    else{
                        //this is not hold situation
                        if(activeCall.data.state != sConfig.callState.idle){
                            var n = sUtils.sendDesktopNotification('Fone Do call', message, function() {
                                window.focus();
                            });
                            self.notifications.push(
                                {
                                    callSid      : message.callSid, 
                                    notification : n
                                });
                        }
                        self.incomingCall(message);    
                    }
                    
                    break;
                case "completed":
                    break;
                case 'answerd':
                    //if this device answered the call then do nothing
                    //if not stop ringing
                    break;
                case "missedCall":
                    sUtils.sendDesktopNotification('Fone Do Missed Call', message, function() {});
                    //add to history 
                    //add to notifications
                    break;
                case "failed":
                    //need to close the modal 
                    //need to add to history
                    break;
                case "busy":
                    break;
                case "no-answer":
                    break;
                case 'un-hold':
                    break;     
                default:
                    throw new Error('No Call handler exist');
                    break;
            }
        }

        self.clearCall = function clearCall(options) {
            var defults = {
                withoutHangup : false
            }
            options = _.merge(defults,options);
            //because clearing the activeCall also clearing the registerToken
            //and I need to set the state to idle in order to distinguish the data on the
            //xms hangup callback
            if(activeCall.data.registerToken){
                var registerToken  = activeCall.data.registerToken;
                activeCall.clear();
                xmsManager.deRegister(registerToken,options.withoutHangup);
            }
            //@TODO remove from conference.participents
        }


        



        self.waitingCallsRemoveItem = function waitingCallsRemoveItem(id,type){
            //clear the timeout
            var item = _.filter(self.waitingCalls,{id : id});
            $timeout.cancel(item.timeout);
            //remove the item from  the array   
            _.remove(self.waitingCalls,{id : id});
            if(type !== 'accept'){
                callMembers.removeMember(id);
            }
        }

        /**
         * makin api call in order to transfer the call
         * @param  {[type]} data [description]
         * @return {[type]}      [description]
         */
        /*self.serverTransferCall = function serverTransferCall(data) {
          

            /*$q.when(activeCall.staticCallModification(sConfig.callAction.forward,{
                To :  data.contact.number ,
                callSid : callSid
            })).then(function(result){
                
            },function(result){
                 console.error(result);
                $rootScope.$broadcast('callModal:close', {
                    status: activeCall.data
                });
            });
        }*/

        self.serverConferenceCall = function serverConferenceCall(data){
            //1.1 add it to conference list 
            self.conference.participants.push(activeCall.data.activeMemberId);
            //1.put active user on hold and 
            self.serverHoldCall(activeCall.data.activeMemberId).then(function(){
                //2.dergister activeuser
                //self.clearCall({withoutHangup : true});
                //3.call another user[problem receiving the callSid]
                xmsManager.generateCall(sUser.getToken(),data.contact.number).then(
                    function(result){
                        //4.add him to modal calls,
                        var memberId= callMembers.addMember({
                            contact : data.contact,
                            status : sConfig.callState.ringing ,
                            state  : 'active',
                            type : sConfig.callType.outgoing,
                            callSid : undefined
                        });
                        
                        //5.add him to conference list
                        self.conference.participants.push(memberId);
                        activeCall.activeMemberId = memberId;
                        
                        ////6.display the modal
                        $rootScope.$broadcast('liveCallModification:conference:add',{
                            status  : 200
                        });        
                        
                    },
                    function(result){
                        console.error('error generating conference call',data)
                    },
                    function(result){
                        activeCall.create({
                            status : sConfig.callType.connecting,
                            type : sConfig.callType.outgoing,
                            registerToken : result.token,
                        });            
                    }
                );

            });    
        }

        self.mergeCalls = function mergeCalls(){
            activeCall.callModification(sConfig.callAction.conference).then(function(result){
                activeCall.roomId = result.data.result.ConferenceRoom;
                _.each(self.conference.participants,function(memberId){
                    var member = callMembers.getMember(memberId);
                    if(member.callSid != activeCall.data.callSid){
                        activeCall.staticCallModification(sConfig.callAction.conference,{callSid : member.callSid});
                        callMembers.updateMember(memberId,{status : 'active'});
                    }
                });
            });
        }

        /*
        self.serverHoldCall = function serverHoldCall(memberId){
            if(!memberId || activeCall.isActiveMember(memberId)){
                $q.reject();
            }
            if(activeCall.isActiveMember(memberId)){
                return $q.when(activeCall.callModification(sConfig.callAction.hold)).then(function(result){
                    self.clearCall()
                    callMembers.updateMember(memberId,{status : sConfig.callState.hold});
                    return ;
                });
            }
        }
        */
        
        self.serverReleaseHold = function serverReleaseHold(memberId){
           $q.when(activeCall.staticCallModification(sConfig.callAction.unhold))
            .then(function(result){
                callMembers.updateMember(memberId,{status : sConfig.callState.active});
            }); 
        }


        


        self.serverHangupCall = function serverHangupCalactiveCall(memberId){
           var callSid = callMembers.getMember(guid).callSid;     
           if(callSid){
                var path = sUtils.parsePath(config.paths.call, {
                    callId: callActive.data.callSid
                });
                var data = {
                    Action: sConfig.callAction.hangup
                }
                return $http.put(path, data).error(function(result) {
                    console.error(result);
                });    
            }else {
                self.clearCall();
            }
        }

        self.serverCancelCall = function serverCancelCall() {

        }

        

       

        self.clearMember = function clearMember (guid){
            callMembers.removeMember(guid);
        }

        

        /**
         * add user to conference room 
         * @param {[type]} number         [description]
         * @param {[type]} conferenceRoom [description]
         */
        self.callUserAndAddToConference = function addUserToConference(number,conferenceRoom){
            console.log('add user to conference');
            if(!number){
                $q.reject();
            }

            if(!self.conference.roomNumber){
                $q.reject();
            }

            var path  = sUtils.parsePath(config.paths.calls);
            var params = {
                To : number,
                From : sUser.phone,
                ConferenceRoom : conferenceRoom
            };
            return $http.post(path,params);
        }

        /**
         * create conference room where the members 
         * get in the room automaticly
         * @param  {[type]} data [description]
         * @return {[type]}      [description]
         */
        self.serverDirectConferenceCall = function serverDirectConferenceCall(data) {
              console.trace('conference call');
              $q.when(activeCall.callModification(sConfig.callAction.conference)).then(function(result){
                    
                    if(!result.data.result.ConferenceRoom){
                        console.error('no conference room',result.data);    
                        return;
                    }

                    self.conference.roomNumber = result.data.result.ConferenceRoom;

                    self.callUserAndAddToConference(data.contact.number,self.conference.roomNumber).then(function(){
                        console.info('conference added');
                      $rootScope.$broadcast('liveCallModification:conference',{
                            message : 'conference added succesflly',
                            status : 200,
                            contact : data.contact
                        })
                    },function(result){
                        $rootScope.$broadcast('liveCallModification:conference',{message : 'conference failed',status : 500})
                        console.error('error adding user to conference room',result);
                    })

              })
        };

       
    }

    Object.defineProperties(callHandler.prototype, {
        conference : {
            get : function (){
                return _conference;
            },
            set : function (value){
                _conference = value
            }
        }
    })
    
    return callHandler;

}());
