var fonedo = fonedo || {};

if (!fonedo['services']) {
    fonedo.services = {};
}


fonedo.services.xmsManager = (function() {
    'use strict';
    var dialogic;
    var registerDeffered ; 
    var mediaPermissionSet = false;
    var registerTry = false;
    var status = 'idle';

    function XmsManager($rootScope,_, $q, $http, $log,xmsProvider) {
    	var self = this;
        
        self.gainAccess = function getAccess(){
            var defferd = $q.defer();
            navigator.getUserMedia({audio: true, video: true},function(){
                defferd.resolve();
            },function(){
                defferd.reject();
            });
            return defferd.promise;    
        };

		self.init = function init(url,port) {
            $log.debug('init');
           self.url = url;
           self.port = port;
           if (!dialogic) {
                dialogic = new xmsProvider();
                dialogic.setHandlers( self.userHandlers );
                $log.log('dialogic object is set');
	        }
        };

        self.generateCall = function generateCall(token,number){
            var defferd = $q.defer();
            self.register(token).then(function(){
                defferd.notify({
                        token : token, number :number,status : 'registered'
                    });
                var result = self.startCall(number);
                if(result == 'ok'){
                    defferd.resolve({
                        token : token, number :number,status : 'call-active'
                    });
                }else{
                    defferd.reject({});
                }
            },function(){
                defferd.reject({'reason' : 'register failed'});
            });

            return defferd.promise;

        }

        self.setXmsPath  = function setXmsPath(url,port){
            if(url){
                self.url = url;
            }
            if(port){
                self.port = port;
            }
        }
        
        self.xmsPath = function xmsPath () {
            var secure = window.location.protocol === 'https:';
            var protocol = secure ? 'wss://' : 'ws://';
            
        	return protocol + self.url + ":" + self.port;
        }
        
        self.register = function register(name,password){
        	
            registerDeffered =  $q.defer();
            $log.debug('register');

        	if(!dialogic.userRegistered){
                dialogic.register(name,self.xmsPath(),'');
            }
            return registerDeffered.promise;
		};

        self.startCall = function startCall(number,type){
            if (!type){
                type = 'audio';
            }
            $log.debug('startCall',arguments);

           var result = dialogic.call(number,type);
           $log.debug(result);
           return result;
        }

        self.endCall = function endCall(){
            dialogic.hangup("User hungup.");
        }

        /**
         * user name == token
         * @param  {[type]} userName [description]
         * @return {[type]}          [description]
         */
        self.deRegister = function deRegister (name,withoutHangup) {
            var ret = dialogic.deRegisterUser(name,withoutHangup);
        }

        /*self.deRegisterWithoutHangup = function deRegisterWithoutHangup(){
            var ret = dialogic.deRegisterUser(name,true);   
        }*/

        self.gainMediaAccess = function gainMediaAccess(audio,video){

            var result = dialogic.acquireLocalMedia({
            'audio': audio,
            'video': video
            });
            if(result == 'ok'){
                $log.debug('acquire local media success');
            }
          
        }

        self.registerBrowserMedia = function registerBrowserMedia(){
            var audioElement = document.getElementById('audio'),
                result;
            
            var config = {
                'remoteAudio': audioElement
            };
            
            result = dialogic.initialize(config);
            
            if ( result === 'ok' ) {
                console.log("user initialize success");
            } else {
                console.log("user initialize fail");
            }
        }

        self.registerSuccess = function registerSuccess(){
        	$log.debug("registerSuccess");
            registerTry = false;
            if(!mediaPermissionSet){
                self.registerBrowserMedia();
                self.gainMediaAccess(true, false);
            }else{
                dialogic.stateTransition(dialogic.getState('idle'));
                registerDeffered.resolve();
            }
        }

        self.registerFail = function registerFail(message,user){
            $log.log("registerFail");	
            if(message == "UserAlreadyRegistered"){
               
                dialogic.deRegisterUser(user.replace('rtc:',''));
                dialogic.userRegistered = false;
                if(!registerTry){
                    registerTry = true;
                    self.register(user.replace('rtc:',''));
                }
            }
            registerDeffered.reject(message);
            
        }

        self.broadcast = function broadcast(type,message){
            $rootScope.$broadcast('xmsHandler:event',{
                initiator : 'xms' ,
                data : message,
                type : type
                }
                );
        }

        self.sendDTMF = function sendDTMF(dtmfNum,duration){
            dialogic.sendDTMF(dtmfNum,duration);
        }
        




        self.ringingHandler = function ringingHandler(message){
        	$log.info("xms callback : ringingHandler",message);	
            self.broadcast('ringing',message);
        }
        self.callConnectedHandler = function callConnectedHandler(message){
            $log.info('xms callback : call connected handler',message);
            self.broadcast('connected',message);
        }
        self.incomingCallHandler = function incomingCallHandler(message){
        	$log.info('xms callback : incomingCallHandler',message);
        }
        self.callHangupHandler = function callHangupHandler(message){
            $log.info('xms callback : callHangupHandler',message);	
            self.broadcast('hangup',message)
        }
        self.disconnectHandler = function disconnectHandler(message){
        	$log.info('xms callback : disconnectHandler',message);	
        }
        self.userMediaSuccessHandler = function userMediaSuccessHandler(message){
            $log.info('xms callback : userMediaSuccessHandler',message);
            
            mediaPermissionSet = true;
            registerDeffered.resolve();

        }
        self.userMediaFailHandler = function userMediaFailHandler(message){
        	$log.info("userMediaFailHandler",message);
        }
        
        /**
        * // The call is now fully established.
        */
        self.remoteStreamAddedHandler = function remoteStreamAddedHandler(message){
        	$log.info('xms callback : remoteStreamAddedHandler',message);
            self.broadcast('remoteStreamAdded',message);
        }
        
        self.messageHandler = function messageHandler(message){
        	$log.info('xms callback : messageHandler',message);
        }
        
        self.infoHandler = function infoHandler(){
        	$log.info('xms callback : infoHandler',message);
        }
        
        self.deRegisterHandler = function deRegisterHandler(message){
        	$log.info('xms callback : deRegisterHandler',message);
        }
      	
        self.userHandlers = {
                'onRegisterOk': self.registerSuccess,
                'onRegisterFail': self.registerFail,
                'onRinging': self.ringingHandler,
                'onConnected': self.callConnectedHandler,
                'onInCall': self.incomingCallHandler,
                'onHangup': self.callHangupHandler,
                'onDisconnect': self.disconnectHandler,
                'onUserMediaOk': self.userMediaSuccessHandler,
                'onUserMediaFail': self.userMediaFailHandler,
                'onRemoteStreamOk': self.remoteStreamAddedHandler,
                'onMessage': self.messageHandler,
                'onInfo': self.infoHandler,
                'onDeregister': self.deRegisterHandler
        }
	}
	
    return XmsManager;

}());
