var fonedo = fonedo || {};

if (!fonedo['services']) {
    fonedo.services = {};
}

fonedo.services.user = (function () {
	'use strict';
	var config = {
		getUserDetails : 'users/:id',
		validateUniqe : 'visitor/user/validate'
	}
	var email,extension,fullName,mobile,phone,role;



	function User (_,$log,$q,$http,sConfig,sUtils,locker,PubNub){
		var self = this;

		self.onPageLoad = function onPageLoad (){
			if(locker.has('userDetails') && locker.has('token')){
				var details = locker.get('userDetails');
				self.setDetails(details);
			
				var token = locker.get('token');	
				sUtils.setAuthHeader([{"WWW-Authenticate" : 'basic ' +  token}]);
			}
		};

		self.setDetails = function setDetails(data){
			if(!data){
				return;
			}
			self.email = data.email;
			self.extension = data.extension;
			self.fullName = data.full_name;
			self.mobile = data.mobile;
			self.phone = data.phone;
			self.role =   sConfig.userRoles[data.role];
			self.user_id = data.user_id;
		};
		
		self.getUserDetails = function get(user_id) {
			if(!user_id){
				$q.reject('missing user id');
				return;
			}
			var path = sUtils.parsePath(config.getUserDetails,{id : user_id});
			return $http.get(path).then(function(result){
				if(!result.data.result){
					$q.reject('missing user data');
				}
				self.setDetails(result.data.result)
				locker.put('userDetails',result.data.result)
				return ;
			});

		};
		
		self.getDefaultRole = function(){
			return sConfig.userRoles['guest'];
		}

		self.logout = function (){
			console.log( PubNub.ngListChannels());
			console.log(sConfig.chanelName + self.getToken());
			PubNub.ngUnsubscribe({channel: sConfig.chanelName + self.getToken()})
			
			locker.clean();
			sUtils.removeAuthHeader(["WWW-Authenticate"]);
			self.setDetails({
				email : "",
				extension : "",
				full_name : "",
				mobile : "",
				phone : "",
				role : "",
				user_id : ""
			});


		}

		self.getToken = function getToken(){
			return locker.get('token');
		}

		self.checkUniqueUser = function checkUniqueUser(type,value){
			console.log('unique',arguments);
			if(!type || !value){
				$q.reject();
			}
			var data = {};
			data[type] = value;
			return $http.post(sUtils.parsePath(config.validateUniqe),data);
		}
	}
	
	Object.defineProperties(User.prototype, {
		email : {
				get : function (){ return email;},
				set : function (value){ email= value;}
		},
		extension : {
				get : function (){ return extension;},
				set : function (value){ extension = value;}
		},
		fullName : {
				get : function (){ return fullName;},
				set : function (value){fullName = value;}
		},
		mobile : {
				get : function (){ return mobile;},
				set : function (value){ mobile = value;}
		},
		phone : {
				get : function (){ return phone;},
				set : function (value){ phone = value;}
		},
		role : {
				get : function (){return role || this.getDefaultRole()},
				set : function (value){role = value}
		}
	});
	return User;
	
}());