var fonedo = fonedo || {};

if (!fonedo['services']) {
    fonedo.services = {};
}



fonedo.services.xmsManagerMock = (function () {
	'use strict';
	function xmsManagerMock ($rootScope,_, $q, $http, $log,xmsProvider){
		var self = this;
		
		self.init = function init(){

		}	

		self.register = function (){
			console.log('mock register');
			var mockDefered = $q.defer();
			_.defer(function(){
				mockDefered.resolve();
			})
			return  mockDefered.promise;
		}
		
		self.generateCall = function (token,number){
			var defferd = $q.defer();
            self.register(token).then(function(){
                defferd.notify({token : token, number :number,status : 'registered'});
                var result = self.startCall(number);
                if(result == 'ok'){
                    defferd.resolve({ token : token, number :number,status : 'call-active'});
                }else{
                    defferd.reject({});
                }
            },function(){
                defferd.reject({'reason' : 'register failed'});
            })
            return defferd.promise;
		}

		self.startCall = function(){
			console.log('mock start call');
			self.broadcast('ringing',{});
			setTimeout(function(){
				self.broadcast('remoteStreamAdded',{});	
			},1000);
			return 'ok';
		}

		self.deRegister = function(name,withoutHangup){
			console.log('mock deRegister');
			if(!withoutHangup){
				self.broadcast('hangup',{});
			}
		}

		self.endCall = function(){
			console.log('mock end call');
			self.broadcast('hangup',{});
		}

		 self.broadcast = function broadcast(type,message){
            $rootScope.$broadcast('xmsHandler:event',{
                initiator : 'xms' ,
                data : message,
                type : type
                }
                );
        }
	}
	return xmsManagerMock;
	
}());
