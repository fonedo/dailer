;(function () {
	'use strict';

	angular.module('foneDo').

		controller('callController', ['$rootScope','$animate','$scope','configService','$timeout','callHandler','activeCall','callMembers','callActions','close','properties', 
			function ($rootScope,$animate,$scope,sConfig,$timeout,callHandler,activeCall,callMembers,callActions,close,properties) {
			
				var self = this;
				
				console.log(properties);
				
				self.properties = properties;
				
				self.display = true;

				self.callOnHold = function(){
					return callMembers.members.length == 1 && callMembers.members[0].state =='on-hold'
				};
				
				self.disableLiveModifcationActions = true;

				self.callIsMerged = false;
				
				self.modalViewState = sConfig.callModalDisplayState.regular;

				self.activeCall  = activeCall.data;

			

				self.calls = [];

				self.callControllType = properties.callControllType;
				
				self.memberId = properties.memberId;

				self.calls = callMembers.members; 
				
				//TODO clear the time out when inside outging call after the other side picked up the phone
				/*if(activeCall.data.type == sConfig.callType.outgoing){
					self.timeout = $timeout(function(){
						self.closeModal();
					},sConfig.ringingTime);
				}*/

				/**
				 * [description]
				 * @param  {[type]} event [description]
				 * @param  {[type]} data  [description]
				 * @return {[type]}       [description]
				 */
				$scope.$on('callModal:remoteStreamAdded',function(event,data){
					//@TODO remove the timeout only for incoming call 
					//or move clearing the timeout to a different function
					$timeout.cancel(self.timeout);
					$scope.$digest();
				});

				/**
				 * listens for event the closes the modal from the xms
				 * @param  {[type]} event [description]
				 * @param  {[type]} data  [description]
				 * @return {[type]}       [description]
				 */
				$scope.$on('callModal:close',function(event,data){
					if(!(self.calls.length > 1)){
						self.closeModal();	
					}
				});

				$scope.$on('callModal:updateCallSid',function(event,data){
					var tmp = _.find(self.calls,{callSid : undefined,state : 'active'});	
					if(tmp){
						tmp.callSid = data.callSid;
					}
					self.disableLiveModifcationActions = false;
					$scope.$digest();
				});

				/**
				 * listens for event after the confrence call is established
				 * @param  {[type]} event [description]
				 * @param  {[type]} data  [description]
				 * @return {[type]}       [description]
				 */
				$scope.$on('liveCallModification:conference:add',function(event,data){
					if(data.status == 200){	
						self.normalModalView();
					}
					self.callControllType = sConfig.callControllType.multipleCalle;
				});
				
				/**
				 * add waiting call to the view
				 * @param  {[type]} data [description]
				 * @return {[type]}      [description]
				 */
				$scope.$on('waitingCalls:add',function(event,data){
					if(self.calls.length > 1){
						self.callControllType = sConfig.callControllType.multipleCalle;
					}
					$scope.$digest();
				});

				/**
				 * remove waiting call from the list
				 * @param  {[type]} data [description]
				 * @return {[type]}      [description]
				 */
				$scope.$on('waitingCalls:remove',function(event,data){
					//remove waiting call from list 
					//if there is no item in the list close the modal
				})
				
				/**
				 * closing the modal
				 * @return {[type]} [description]
				 */
				self.closeModal = function closeModal(){
					if(activeCall.data.type == undefined || activeCall.data.type == sConfig.callType.incoming){
						callHandler.ringingHandler('pause');
					}
					$timeout.cancel(self.timeout);
					close();	
				} 
				
				
				/**
				 * accept incoming call
				 * @return {[type]} [description]
				 */
				self.acceptSingleCall = function acceptSingleCall(memberId){
					if(!memberId){
						memberId = self.memberId
					}
					$timeout.cancel(self.timeout);
	 				callHandler.acceptCall(memberId);
					self.callControllType = sConfig.callControllType.call;
				}

				/**
				 * reject single 
				 * @return {[type]} [description]
				 */
				self.rejectSingleCall = function rejectSingleCall (){
					self.closeModal();
				}

				
				
				self.waitingCallReject = function waitingCallReject (memberId){
					if(activeCall.isActiveMember(memberId)){
						callHandler.clearCall();
						callMembers.removeMember(memberId);
					}else{
						callHandler.waitingCallsRemoveItem(memberId);
					}
					switch(callMembers.members.length){
						case 0 :
							self.closeModal();
						break;
						case 1 :
							if(callMembers.members[0].status == sConfig.callState.active){
								self.callControllType =	sConfig.callControllType.call;
							}else if(callMembers.members[0].status == sConfig.callState.merged ){
								callMembers.members[0].status = sConfig.callState.active;
								self.callControllType =	sConfig.callControllType.call;
							}
							else{	
								callMembers.members[0].status = sConfig.callState.answering;
								self.callControllType = sConfig.callControllType.answer;
							}
						break;
					}

				}

				self.waitingCallAccept = function waitingCallAccept (callSid){
					debugger;
				}

				self.waitingCallHold = function waitingCallHold (memberId){
					self.holdCall(memberId);	
				}

				/**
				 * holds the active call 
				 * @param  {[type]} callSid [description]
				 * @return {[type]}         [description]
				 */
				self.holdCall = function holdCall (memberId){
					if(!memberId){
						memberId = self.memberId
					}
					var member = callMembers.getMember(memberId);
					if(member.status != sConfig.callState.hold){
						callHandler.serverHoldCall(memberId).then(function(){
							self.callOnHold = true;
							$scope.$digest();
						});
					}else{
						callHandler.serverReleaseHold(memberId).then(function(){
							self.callOnHold = false;
							$scope.$digest();	
						});
					}
				}

				/**
				 * set up transfer state 
				 * prepare the ui for selecting another number
				 * @return {[type]} [description]
				 */
				self.transferView = function transferCall (memberId){
					$timeout.cancel(self.timeout);
					callHandler.ringingHandler('pause');
					activeCall.data.status = sConfig.callState.transfer;
					callMembers.updateMember(memberId,{status:sConfig.callState.transfer});
					properties.stateInfoDisplay = "Transfer | ";
					self.dockModalView();
				}
				
				/**
				 * [mergeCall description]
				 * @return {[type]} [description]
				 */
				self.mergeCall = function mergeCall (){
					callHandler.mergeCalls();
					self.callIsMerged = true;
				}

				/**
				 * cancel any live call modification operation
				 * @return {[type]} [description]
				 */
				self.cancelLiveCallModification = function cancelLiveCallModification(){
					properties.stateInfoDisplay = "";
					activeCall.state = sConfig.callState.active;
					var members = callMembers.find({status : sConfig.callState.transfer});
					_.each(members,function(item){
						if(callActive.isActiveMember(item.guid)){
							callMembers.update(item.guid,{status : sConfig.callState.active});
						}else{
							callMembers.update(item.guid,{status : sConfig.callState.waiting});
						}
					});
					self.normalModalView();
				}

				self.conferenceView = function conferenceCall (){
					$timeout.cancel(self.timeout);
					activeCall.status = sConfig.callState.conference;
					properties.stateInfoDisplay = "Conference | ";
					self.dockModalView();
				}

				self.moveToAnotherDevice = function moveToAnotherDevice (){
					console.error("Move To Another Device");
				}

				self.muteCall = function muteCall (){
					console.error('mute call');
				}
				
				/**
				 * hangup the call
				 * @return {[type]} [description]
				 */
				self.hangupCall = function hangupCall (){
					self.closeModal();
				}

				/**
				 * docking the modal to the top of the window
				 * @return {[type]} [description]
				 */
				self.dockModalView = function dockModalView (){
					self.modalViewState = sConfig.callModalDisplayState.dock;
					var modalElement = angular.element(document.getElementById("callModal"));
					var modalFadeElement = angular.element(document.getElementById("callModalFade"));
					
					$animate.addClass(modalElement,'minor-display');
					$animate.addClass(modalFadeElement,'minor-display');
				}

				/**
				 * displaying the modal regular (with dark background and the modal is in the middle)
				 * @return {[type]} [description]
				 */
				self.normalModalView = function normalModalView (){
					self.modalViewState = sConfig.callModalDisplayState.regular;
					var modalElement = angular.element(document.getElementById("callModal"));
					var modalFadeElement = angular.element(document.getElementById("callModalFade"));
					
					$animate.removeClass(modalElement,'minor-display');
					$animate.removeClass(modalFadeElement,'minor-display');
				}

				self.minimizeModalView  = function minimizeModalView (){
				}

				self.displaySmartCallerData = function displaySmartCallerData(){
					return self.calls.length <= 1; 
				}


		}]);
}());