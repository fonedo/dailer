var fonedo = fonedo || {};

if (!fonedo['services']) {
    fonedo.services = {};
}
fonedo.services.utils = (function() {
    'use strict';
    var loadedScripts = [];

    function Utils(_,$q, sConfig,$http) {
        var self = this;
        var lut = []; 
        for (var i=0; i<256; i++) { 
            lut[i] = (i<16?'0':'')+(i).toString(16); 
        }

        self.generateMobileNumber = function generateMobileNumber(number,countryPrefix){
            if(_.isEmpty(number) || _.isEmpty(countryPrefix)){
                return '';
            }
            if(typeof number != 'string'){
                number = number.toString();
            }
            if(countryPrefix == 972 && number[0] == '0'){
                number = number.slice(1);
            }
            return self.onlyNumbers(countryPrefix + number);
        }
        /**
         * generate guid compatible with rfc4122
         * @return {[type]} [description]
         */
        this.guid = function guid(){
          var d0 = Math.random()*0xffffffff|0;
          var d1 = Math.random()*0xffffffff|0;
          var d2 = Math.random()*0xffffffff|0;
          var d3 = Math.random()*0xffffffff|0;
          return lut[d0&0xff]+lut[d0>>8&0xff]+lut[d0>>16&0xff]+lut[d0>>24&0xff]+'-'+
            lut[d1&0xff]+lut[d1>>8&0xff]+'-'+lut[d1>>16&0x0f|0x40]+lut[d1>>24&0xff]+'-'+
            lut[d2&0x3f|0x80]+lut[d2>>8&0xff]+'-'+lut[d2>>16&0xff]+lut[d2>>24&0xff]+
            lut[d3&0xff]+lut[d3>>8&0xff]+lut[d3>>16&0xff]+lut[d3>>24&0xff];
        }

        /**
         * convert json object ot base 64
         * @param {[type]} object [description]
         */
        this.jsonToBase64 = function jsonToBase64(object) {
            return object;
            //return btoa(JSON.stringify(object));
        }

        this.parsePath = function parsePath(pattern, params) {
            if (params) {
                _.each(params, function(value, key) {
                    pattern = pattern.replace(":" + key, value);
                });
            }
            return sConfig.restApi + pattern;
        }

        this.parsePositionRawData = function parsePositionRawData(w) {
            var raw = {
                width: w.innerWidth,
                height: w.innerHeight,
                maxWidth: w.document.body.scrollWidth,
                maxHeight: w.document.body.scrollHeight,
                posX: w.scrollX || w.pageXOffset || w.document.documentElement.scrollLeft,
                posY: w.scrollY || w.pageYOffset || w.document.documentElement.scrollTop
            };

            // remove but log overscroll
            if (raw.posX < 0) {
                raw.posX = 0;
                raw.overscrollLeft = true;
            } else if (raw.posX + raw.width > raw.maxWidth) {
                raw.posX = raw.maxWidth - raw.width;
                raw.overscrollRight = true;
            }

            if (raw.posY < 0) {
                raw.posY = 0;
                raw.overscrollTop = true;
            } else if (raw.posY + raw.height > raw.maxHeight) {
                raw.posY = raw.maxHeight - raw.height;
                raw.overscrollBottom = true;
            }
            raw.hasOverscroll = raw.overscrollTop || raw.overscrollBottom || raw.overscrollLeft || raw.overscrollRight;

            return raw;
        }

        this.setAuthHeader = function setAuthHeader (pair) {
        	if(!pair){
        		return;
        	}
        	for (var i = 0 ;i < pair.length;i++){
        		var key = Object.keys(pair[i])[0];
	            $http.defaults.headers.common[key] = pair[i][key];
        	}
        }

        this.removeAuthHeader = function removeAuthHeader (keys){
            if(!keys || !keys.length){
                return;
            }
            for (var i = 0 ;i < keys.length;i++){
                delete $http.defaults.headers.common[keys[i]];
            }   
        }

        /**
         * load script async
         * @param  {[type]} url [description]
         * @return {[type]}     [description]
         */
        this.loadAsyncScript = function loadAsyncScript (url){
            var deffered = $q.defer();
            if(loadedScripts.indexOf(url) !== -1 ){
                deffered.resolve({alreadyLoaded : true});
            }
            var scriptElement = document.createElement('script'),
                f = false,
                a;

            scriptElement.src = url,
            scriptElement.async = true;
            scriptElement.onload = scriptElement.onreadystatechange = function (){
                a = this.readyState;
                if(f || a && a != 'complete' && a != 'loaded'){
                    return;
                }
                f = true;
                deffered.resolve();
            }
            document.getElementsByTagName('head')[0].appendChild(scriptElement);

            return deffered.promise;
        };

        this.sendDesktopNotification = function sendDesktopNotification(title,message,onClickCallback) {
            if(window.Notification && Notification.permission !== "denied"){
                var n = new Notification(title,{body : JSON.stringify(message),tag : "fonedo",icon:'/assets/images/favicon.png'});
                if(onClickCallback){
                    n.onclick = onClickCallback;
                }
                n.onshow = function(){
                    setTimeout(n.close.bind(n),sConfig.answeringTime);
                }
                return n;
            }        
        };

        this.requestNotificationPermission = function requestNotificationPermission(){
            if(window.Notification && Notification.permission !== 'granted'){
                Notification.requestPermission(function(status){
                    if(Notification.permission !== status){
                        Notification.permission = status;
                    }
                });
            }
        }

        this.onlyNumbers = function onlyNumbers(value){
            if(!value){
                return '';
            }
            if(typeof value == 'number'){
                return number;
            }
            var result = value.match(/\d+/ig);
            if(result){
                return result.join('');    
            }
            return '';
        }
    }
    return Utils;
})();
