var fonedo = fonedo || {};

if (!fonedo['services']) {
    fonedo.services = {};
}

/**
 * member status 
 * active,not-active,waiting,on-hold 
 * 	
 * @param  {Array}  ) {	'use       strict';	var _members [description]
 * @return {[type]}   [description]
 */
fonedo.services.callMembers = (function() {
	'use strict';
	var _members = [];
	
    var callMembers =function(_,$q,sConfig,sUtils) {
		var self = this;
		
		self.updateMember = function (guid,newData){
			if(newData.guid){
				newData.guid = undefined;
			}
			var data = _.find(_members,{guid : guid});
			data  = _.merge(data,newData);
		}

		self.addMember = function (member){
            var data = _.merge(member,{guid : sUtils.guid()});
            _members.push(data);
            return data.guid;
        }
        
        self.removeMember = function (guid){
            _.remove(_members,{guid : guid});
        }

        self.getMember = function(guid){
            return _.find(_members,{guid : guid});
        }

        self.find = function find (params){
        	return _.where(_members,params);
        }

        self.removeAll = function(){
        	_members = [];
        }

        

    };

	Object.defineProperties(callMembers.prototype, {
		members : {
			get : function() {
				return _members;
			}
		}
	})

	return callMembers;
}());