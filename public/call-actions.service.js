var fonedo = fonedo || {};

if(!fonedo['services']){
	fonedo.services = {};
}

fonedo.services.callActions = (function() {
	'use strict';
	var callActions = function(_,$rootScope,$q,$http,sConfig,sUtils,activeCall,callMembers){
		var self =this;

		self.transferCall = function transferCall(contact){
			var params = {};
		  	var member = callMembers.find({status : sConfig.callState.transfer});
            if(member.length != 1 || ! member[0].callSid ){
                console.error(member,'error imposible situation');
                $q.reject();
            }
            var callSid = member[0].callSid;
			params['To'] = contact.number;
			return self.liveCallModification(callSid,sConfig.callAction.forward,params).finally(
				function(){
					 //@todo change action to callAction:transfer:completed
					 //trigger to clear the call ;
				 	$rootScope.$broadcast('callModal:close', {
                    		status: activeCall.data
            		});
				});
		}

		self.holdCall = function holdCall (member,params){
			if(!member.callSid){
				$q.reject();
			}
			return self.liveCallModification(member.callSid,sConfig.callAction.hold,params).thend(function(result){
				var result = result.data.result;	
				callMembers.updateMember(member.memberId,{
					status : sConfig.callState.hold,
					conference : result.ConferenceRoom
				});
			});
		}

		self.unHoldCall = function unHoldCall (callSid,params){
			return self.liveCallModification(callSid,sConfig.callAction.unhold,params);
		}

		self.addCall = function addCall (callSid,params){
			return self.liveCallModification(callSid,sConfig.callAction.conference,params);
		}

		self.rejectCall = function rejectCall (callSid,params){
			return self.liveCallModification(callSid,sConfig.callAction.reject,params);
		}
		
		self.hangupCall  = function hangupCall (callSid,parmas){
			return self.liveCallModification(callSid,sConfig.callAction.hangup,params);
		}

		/**
         * api call in order to create conference room (for hold/conference live call modifications)
         * after the room is generated you can add or remove contacts from the room 
         * @return {[type]} [description]
         */
        self.liveCallModification = function liveCallModification(callSid,action,params){
            if(!callSid || !actions){
            	console.error('missing data for live call modifications',arguments);
            	$q.reject();
            }

            var path = sUtils.parsePath(config.paths.call, {
                callId: callSid
            });

            var data = {
                'Action': action
            };
            
            if(params){
                data = _.merge(data,params);
            }

            return $http.put(path,data);
        }
	}
	return callActions;
	
}());