var fonedo = fonedo || {};

if (!fonedo['services']) {
    fonedo.services = {};
}

fonedo.services.config = (function() {
        var Credentials = {
            pubNub : {}
        };
        
        var roles =  [
                'public',
                'guest',
                'user',
                'admin'
            ];
        var  accessLevels = {
                'public'  : "*",
                'guest' : ['guest'],
                'user'  : ['user','admin'],
                'admin'  : ['admin']   
            };

       var userRoles = (function setUserRoles (roles){
            var bitMask = '01';
            var userRoles = {};
            for(var role in roles){
                var intCode = parseInt(bitMask,2);
                userRoles[roles[role]] = {
                    bitMask : intCode,
                    title : roles[role]
                };
                bitMask = (intCode << 1).toString(2)
            }
            return userRoles;
        }(roles));

        var accessLevels = (function buildAccessLevels(accessLevelDeclarations,userRoles){
            var accessLevels = {};
            for(var level in accessLevelDeclarations){
                if(typeof accessLevelDeclarations[level] == 'string'){
                    if(accessLevelDeclarations[level] == '*'){
                        var resultBitMask = '';
                        for(var role in userRoles)
                        {
                            resultBitMask +='1';
                        }
                        accessLevels[level] = {
                            bitMask : parseInt(resultBitMask,2)
                        }
                    }
                    else{
                        console.log("Access Control Error: Could not parse '" + accessLevelDeclarations[level] + "' as access definition for level '" + level + "'");       
                    }
                }
                else{
                    var resultBitMask = 0;
                    for(var role in accessLevelDeclarations[level])
                    {
                        if(userRoles.hasOwnProperty(accessLevelDeclarations[level][role])){
                            resultBitMask = resultBitMask | userRoles[accessLevelDeclarations[level][role]].bitMask;
                        }
                        else{
                            console.log("Access Control Error: Could not find role '" + accessLevelDeclarations[level][role] + "' in registered roles while building access for '" + level + "'");
                        }
                    }    
                    accessLevels[level] = {
                        bitMask: resultBitMask
                    };
                }
            }
            return accessLevels;
        }(accessLevels,userRoles));

        var setPubNubCredentials = function setPubNubCredentials(publish_key,subscribe_key){
            Credentials.pubNub.publish_key = publish_key
            Credentials.pubNub.subscribe_key = subscribe_key;
        };

    function Config($location,$log) {
        $log.info('Config Service initilaized');
        this.flags = [
                     {
                        class : 'us',
                        shortLabel : 'US',
                        label : 'USA +1',
                        value : "1"
                    },   
                    {
                        class : 'ca',
                        shortLabel : 'CA',
                        label : 'Canada +1',
                        value : '1'
                    }
                ]
        
        this.ringingTime = 15000;
        
        this.answeringTime = 15000;
                
        if ($location.$$host.indexOf('dev') > -1) {

            this.xmsUrl = '54.152.191.172';
            //this.xmsUrl = '54.152.232.25';

            
            this.flags.push(
                    {
                        class : 'il',
                        shortLabel : 'IL',
                        label : 'Israel +972',
                        value : "972"
                    });
                    
                
            this.restApi = '//localhost.fone.do:' + $location.$$port + '/api/dev/';
            //this.restApi = 'http://admin:FoneDo@192.168.100.62:8080' + '/api/v1/debug/';
            
            //this.restApi = 'http://admin:FoneDo@qa.fone.do/api/v1/';
            //this.restApi = 'http://admin:FoneDo@lamptest.fone.do/api/v1/';

            
            this.ringingTime = 150000;
        
            this.answeringTime = 150000;

            this.demoUsers = true;
        } 
        else if($location.$$host.indexOf('qa.fone') > -1) {
            this.xmsUrl = '54.152.191.172';
            this.restApi = '//qa.fone.do/api/v1/';
            this.demoUsers = false;
            
            this.flags.push(
                    {
                        class : 'il',
                        shortLabel : 'IL',
                        label : 'Israel +972',
                        value : "972"
                    });
        }
        else if($location.$$host.indexOf('aoni') > -1){
            //this.xmsUrl = '54.164.195.147';
            this.xmsUrl = '54.152.232.25';
        	this.restApi = 'http://aoni.api.com:8080/api/v1/';
            this.demoUsers = true;
        }
        else if($location.$$host.indexOf('lamptest.fone.do') > -1) {
            //this.xmsUrl = '54.164.195.147';
            this.xmsUrl = '54.152.232.25';
            this.restApi = 'http://admin:FoneDo@lamptest.fone.do/api/v1/';
            this.demoUsers = false;
        }
        else if($location.$$host.indexOf('test') > -1){
            this.xmsUrl = '54.152.191.172';
            this.restApi = '/test/api/v1/';
        }
        else if($location.$$host.indexOf('local') > -1){
            //this.xmsUrl = '54.152.232.25';
            //this.restApi = '//localhost.fone.do:' + $location.$$port + '/api/dev/';
            this.xmsUrl = '54.152.191.172';
            this.restApi = "http://admin:FoneDo@qa.fone.do/";
            //this.restApi = 'https://admin:FoneDo@lamptest.fone.do/api/v1/';
        }
        else{
            //this.xmsUrl = '54.164.195.147';
            //this.xmsUrl = '54.152.232.25';
            this.xmsUrl = '54.152.191.172';
            this.restApi = "http://qa.fone.do/";
        }

        this.getPubNubCredentials = function getPubNubCredentials(){
            return Credentials.pubNub;
        }

        //'/src/app/components/call/templates/'
        this.callModalTemplateSource = '/';

        this.callState = {
            idle : "idle",
            connecting : 'connecting',
            ringing : 'ringing',
            answerd : "answerd",
            process : 'process',
            completed : "completed",
            missedCall : 'missedCall',
            failed : 'failed',
            busy : 'busy',
            noAnwer : "noAnswer",
            transfer : 'transfer',
            conference : 'conference',
            hold : 'hold',
            waiting : 'waiting',
            answering : 'answering',
            active : 'active',
            merged : 'merged'
        }

        this.callType = {
            incoming : 'incoming',
            outgoing : 'outgoing'
        }

        this.callAction = {
            forward : "forward",
            hangup : "hangup",
            conference : 'conference',
            hold : 'hold',
            unhold : 'unhold',
            reject : 'reject'
        }
        
            

        this.callModalDisplayState = {
            normal : 'normal',
            dock : 'dock',
            minimize : 'minimize',
            maximize : 'maximize'
        }

        this.callControllType = {
            call : 'call',
            answer : 'answer',
            multipleCalle : "multipleCalle",
            conference : "conference"
        }

        this.outgoingCallIdentificationPrefix = 'xms';

        this.emptyCallSid = '1111'

        this.autoCompleteKeyPressTrigger = 2;
        
        this.sessionTimeout = 30; //minutes
        
        this.marktingSiteMainVideo = 'BUaFugdLWyE';
        
        this.userRoles = userRoles;
        
        this.accessLevels = accessLevels;

        this.xmsPort = 1080;

        this.chanelName = ""; //fd_router
        
        this.mockData = {
            departmentsList: [{
                id: 1,
                name: 'Customer Support'
            }, {
                id: 2,
                name: 'Sales and Marketing'
            }, {
                id: 3,
                name: 'Accounting'
            }]
        };

        this.visitorTokenExpiredTime = 15 * 60 * 1000;


        this.sendCodeAttemptsLimit = 2;

        this.loginTryLimit = 3;

    
    }

    return {
        setPubNubCredentials : setPubNubCredentials,
        getAccessLevels  : function(){return accessLevels},
        $get : ['$location','$log',function($location,$log){
            return new Config($location,$log);
        }]
    }
})();
