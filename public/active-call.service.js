var fonedo = fonedo || {};

if(!fonedo['services']){
	fonedo.services = {};
}

fonedo.services.activeCall =(function () {
	'use strict';
	 var  config = {
            paths: {
                call: 'call/:callId'
         	}
        }

    function ActiveCall (_,$q,$http,sConfig,sUtils){
		var self = this;
		self._data = {
			status  : sConfig.callState.idle
		};

		self.create = function create(properties){
            self.clear();
			if(properties.state){
				self.state = properties.state;
			}

			if(properties.callSid){
				self.callSid = properties.callSid;
			}

			if(properties.type){
				self.type = properties.type;
			}

			if(properties.registerToken){
				self.registerToken = properties.registerToken;
			}
            
            if(properties.status){
                self.status = properties.status;
            }

            if(properties.activeMemberId){
                self.activeMemberId = properties.activeMemberId;
            }
        }

		self.clear = function clear (){
			self.type = undefined;
			self.status = sConfig.callState.idle;
			self.callSid = undefined;
			self.registerToken = undefined;
			self.roomId = undefined;
            self.state = undefined;
            self.activeMemberId = undefined;
        }

		self.isActiveCall = function isActiveCall(callSid){
			return callSid === self._data.callSid;
		}

        self.isActiveMember = function isActiveMember(activeMemberId){
            return self._data.activeMemberId === activeMemberId;
        }
    }

    Object.defineProperties(ActiveCall.prototype, {
    	data : {
    		get : function (){
    			return this._data;
    		}
    	},
        state: {
            //get : function (){return this.state;},
            set: function(value) {
                this._data.state = value
            }
        },
        callSid: {
        	///get : function (){return this.callSid;},
            set: function(value) {
                this._data.callSid = value;
            }
        },
        type: {
        	//get : function (){return this.type;},
            set: function(value) {
                this._data.type = value;
            }
        },
        registerToken : {
        	//get : function (){return this.registerToken;},
            set : function(value){
                this._data.registerToken = value;
            }
        },
        roomId : {
        	set : function (value){
        		this._data.roomId  = value;
        	}
        },
        activeMemberId : {
            set : function (value){
                this._data.activeMemberId = value;
            }
        },
        status : {
            set : function (value){
                this._data.status = value;
            }
        }
    })
	
    return ActiveCall;
	
}());