var fonedo = fonedo || {};

if(!fonedo['services']){callHandler
	fonedo.services = {};
}
;(function () {
	'use strict';
	var app = angular.module('foneDo.call',[]);

app
	.service('userService',['_','$log','$q','$http','configService', 'utilsService','locker','PubNub',fonedo.services.user])
	.provider('configService',fonedo.services.config)
	.service('xmsManager',['$rootScope','_','$q','$http','$log','xmsProvider',fonedo.services.xmsManager])
	//.service('xmsManager',['$rootScope','_','$q','$http','$log','xmsProvider',fonedo.services.xmsManagerMock])
	.service('utilsService',['_','$q','configService','$http',fonedo.services.utils])
	.service('callHandler',['$rootScope','_','$q','$http','$log','$timeout','ModalService','xmsManager','userService','configService','PubNub','utilsService','activeCall','callMembers',"callActions",fonedo.services.callHandler])
	.service('callLogService',['_','$q','$http','configService','utilsService','userService',fonedo.services.callLog])
	.service('activeCall',['_','$q','$http','configService','utilsService',fonedo.services.activeCall])
	.service('callMembers',['_','$q','configService','utilsService',fonedo.services.callMembers])
	.service("callActions",['_','$rootScope','$q','$http','configService','utilsService','activeCall','callMembers',fonedo.services.callActions])
	.factory('_',function(){
		return window._;
	})
	.value('xmsProvider',function(){
		return new window.Dialogic ();
	})
	;

	
}());


